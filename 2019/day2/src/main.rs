use std::fs;

fn main() {
    let contents = fs::read_to_string("data/input".to_string())
        .expect("Something went wrong reading the file");
    let contents = remove_whitespace(&contents);
    let integers: Vec<i32> = contents
        .split(",")
        .map(|x| x.parse::<i32>().expect("Not a number!"))
        .collect();

    part1(&integers);
    part2(&integers);
}

fn remove_whitespace(s: &String) -> String {
    s.chars().filter(|c| !c.is_whitespace()).collect()
}

fn part1(instructions: &Vec<i32>) {
    println!(
        "Part1 output: {}",
        calculate_output(12, 2, &mut instructions.clone())
    );
}

fn part2(instructions: &Vec<i32>) {
    for x in 0..99 {
        for y in 0..99 {
            let result = calculate_output(x, y, &mut instructions.clone());

            if result == 19690720 {
                println!("Part2 noun, verb, output: {}, {}, {}", x, y, result);
                println!("Part2 solution (100 * noun, verb): {}", 100 * x + y);
                return;
            }
        }
    }
}

fn calculate_output(noun: i32, verb: i32, instructions: &mut Vec<i32>) -> i32 {
    instructions[1] = noun;
    instructions[2] = verb;

    for i in { 0..instructions.len() }
        .filter(|n| n % 4 == 0)
        .collect::<Vec<usize>>()
    {
        match instructions[i] {
            1 => {
                let value_one = instructions[instructions[i + 1] as usize];
                let value_two = instructions[instructions[i + 2] as usize];
                let store = instructions[i + 3] as usize;
                instructions[store] = value_one + value_two;
            }
            2 => {
                let value_one = instructions[instructions[i + 1] as usize];
                let value_two = instructions[instructions[i + 2] as usize];
                let store = instructions[i + 3] as usize;
                instructions[store] = value_one * value_two;
            }
            99 => break,
            _ => panic!("Invalid instruction"),
        }
    }

    return instructions[0];
}
