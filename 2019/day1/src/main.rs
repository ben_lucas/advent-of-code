use std::fs;

fn main() {
    let filename = "data/input";
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    part1(&contents);
    part2(&contents);
}

fn part1(content: &String) {
    let total: f32 = content
        .lines()
        .map(|line| fuel_for_module(line.parse::<f32>().expect("Not a number!")))
        .sum();

    println!("Total part 1 required: {}", total);
}

fn part2(content: &String) {
    let total: f32 = content
        .lines()
        .map(|line| fuel_requirement_recursive(line.parse::<f32>().expect("Not a number!")))
        .sum();

    println!("Total part 2 required: {}", total);
}

fn fuel_for_module(module: f32) -> f32 {
    { module / 3.0 }.floor() - 2.0
}

fn fuel_requirement_recursive(module: f32) -> f32 {
    let fuel = fuel_for_module(module);

    if fuel > 0.0 {
        return fuel + fuel_requirement_recursive(fuel);
    }

    return 0.0;
}
